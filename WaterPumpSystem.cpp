#include "WaterPumpSystem.h"
#include <Arduino.h>

WaterPumpSystem::WaterPumpSystem() 
    : waterPumps{WaterPump(2), WaterPump(3), WaterPump(4), WaterPump(5)},
      moistureSensors{MoistureSensor(A0), MoistureSensor(A1), MoistureSensor(A2), MoistureSensor(A3)},
      distanceSensor(),
      firebaseManager(FirebaseManager::getInstance()), 
      timeLastExecute(0), 
      freq(15) {}

void WaterPumpSystem::setup() {
    wifiManager.initialize();
    delay(10000);
    for (int i = 0; i < 4; i++) {
        waterPumps[i].initialize();
        moistureSensors[i].initialize();
    }
    distanceSensor.initialize();
    delay(5000);
    firebaseManager.initialize();
}

void WaterPumpSystem::loop() {
    wifiManager.checkConnection();

    unsigned long currentTime = millis();
    if (currentTime - timeLastExecute > humidityTime() || timeLastExecute == 0) {
        noInterrupts();
        testMoistureLevel();
        timeLastExecute = currentTime;
        interrupts();
    }
    checkRelayTimeout();
}

void WaterPumpSystem::testMoistureLevel() {
    lastCheck();
    int diposit = distanceSensor.getAverageDistance();
    firebaseManager.sendDiposit(diposit);
    for (int i = 0; i < 4; i++) {
        int moistureLevel = moistureSensors[i].read();
        waterPumps[i].updateHumidity(moistureLevel);
        firebaseManager.sendData(i, moistureLevel);
        if ((diposit != -1) && (diposit != 0) && (diposit != 1) && (moistureLevel > firebaseManager.getMoistureLevel(i))) {
            waterPumps[i].activate();
        } else if (moistureLevel <= firebaseManager.getMoistureLevel(i)) {
            waterPumps[i].deactivate();
        }
    }
    delay(5000);
    firebaseManager.updateConfig();
}

void WaterPumpSystem::checkRelayTimeout() {
    for (int i = 0; i < 4; i++) {
        waterPumps[i].checkTimeout();
    }
}

bool WaterPumpSystem::checkOpenRelay() {
    for (int i = 0; i < 4; i++) {
        if (waterPumps[i].isActive()) {
            return true;
        }
    }
    return false;
}

long unsigned WaterPumpSystem::humidityTime() {
    return 60000 * freq;
}

void WaterPumpSystem::lastCheck() {
    firebaseManager.recordLastCheck();
}
