#include "FirebaseManager.h"

void FirebaseManager::initialize() {
    Firebase.begin(SECRET_FBHOST, SECRET_DBSF, SECRET_SSIDF, SECRET_PASSF);
    Firebase.reconnectWiFi(true);
}

bool FirebaseManager::retryOperation(bool (FirebaseManager::*operation)(), int maxRetries) {
    int attempts = 0;
    while (attempts < maxRetries) {
        if ((this->*operation)()) {
            return true;
        }
        attempts++;
        delay(1000); // Espera abans de reintentar
    }
    return false;
}

bool FirebaseManager::setIntRetry() {
    return Firebase.setInt(fbdo, path + "/Diposit", retryIntValue);
}

bool FirebaseManager::setBoolRetry() {
    return Firebase.setBool(fbdo, path + "/bombes/" + retryByteValue, retryBoolValue);
}

bool FirebaseManager::getIntRetry() {
    return Firebase.getInt(fbdo, path + "/frecuencia");
}

void FirebaseManager::sendDiposit(int diposit) {
    retryIntValue = diposit;
    retryOperation(&FirebaseManager::setIntRetry);
}

void FirebaseManager::sendData(byte sensor, int value) {
    int torreta = sensor < 2 ? 1 : 2;
    retryIntValue = value;
    path = "/torretes/Sensor_Humitat/" + String(torreta) + "/" + String(sensor);
    retryOperation(&FirebaseManager::setIntRetry);
}

byte FirebaseManager::getFreq() {
    retryOperation(&FirebaseManager::getIntRetry);
    return fbdo.intData();
}

int FirebaseManager::getMoistureLevel(int index) {
    path = "/torretes/Config_Humidity/" + String(index);
    retryOperation(&FirebaseManager::getIntRetry);
    return fbdo.intData();
}

void FirebaseManager::setWaterPumpStatus(byte pump, bool status) {
    retryByteValue = pump;
    retryBoolValue = status;
    retryOperation(&FirebaseManager::setBoolRetry);
}

void FirebaseManager::recordLastCheck() {
    // Implementar la lògica de registre de data
}

void FirebaseManager::updateConfig() {
    // Implementar la lògica d'actualització de la configuració
}
