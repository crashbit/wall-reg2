#ifndef WATERPUMPSYSTEM_H
#define WATERPUMPSYSTEM_H

#include "WaterPump.h"
#include "DistanceSensor.h"
#include "MoistureSensor.h"
#include "FirebaseManager.h"
#include "WiFiManager.h"

class WaterPumpSystem {
public:
    WaterPumpSystem();
    void setup();
    void loop();

private:
    WaterPump waterPumps[4];
    DistanceSensor distanceSensor;
    MoistureSensor moistureSensors[4];
    FirebaseManager& firebaseManager;
    WiFiManager wifiManager;
    unsigned long timeLastExecute;
    byte freq;
    
    void initialize();
    void testMoistureLevel();
    void checkRelayTimeout();
    bool checkOpenRelay();
    long unsigned humidityTime();
    void lastCheck();
};

#endif
