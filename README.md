# Sistema de Reg Automàtic

Aquest projecte implementa un sistema de reg automàtic per a plantes utilitzant una placa Arduino, sensors d'humitat, un sensor de distància, i una connexió a la base de dades en temps real de Firebase. El sistema controla quatre relés que activen les bombes d'aigua basant-se en els nivells d'humitat del sòl i el nivell del dipòsit d'aigua.

## Característiques

- **Control de Relés:** El sistema controla quatre relés per activar o desactivar les bombes d'aigua segons els nivells d'humitat.
- **Sensors d'Humitat:** Utilitza sensors d'humitat per mesurar la humitat del sòl.
- **Sensor de Distància:** Utilitza un sensor de distància per mesurar el nivell del dipòsit d'aigua.
- **Connexió a Firebase:** Envia dades dels sensors a Firebase i recupera configuracions des de Firebase.
- **Protecció de Relés:** Protegeix els relés assegurant-se que es desactivin si la humitat no augmenta en un període de temps definit.
- **Reintents en Connexió:** Implementa un mecanisme de reintents per assegurar la fiabilitat en la connexió amb Firebase.

## Requeriments

- Arduino Uno WiFi Rev2
- Sensors d'humitat (4)
- Sensor de distància
- Relés (4)
- Connexió WiFi
- Compte de Firebase amb una base de dades en temps real configurada

## Instal·lació

1. **Cloneu el repositori:**
   ```sh
   git clone https://gitlab.com/usuari/wall-reg2.git
   cd wall-reg2

2. **Configureu les vostres credencials de Firebase:**
   - Creeu un fitxer `arduino_secrets.h` a l'arrel del projecte amb les vostres credencials de Firebase i WiFi:
     ```cpp
     #define SECRET_SSIDF "el_vostre_ssid"
     #define SECRET_PASSF "la_vostra_contrasenya"
     #define SECRET_DBSF "la_vostra_base_de_dades"
     #define SECRET_FBHOST "el_vostre_host_de_firebase"
     ```

3. **Carregueu el codi a la vostra placa Arduino:**
   - Utilitzeu l'Arduino IDE per carregar el codi a la placa.

## Ús

- **Inicialització:** Engegueu el sistema connectant la placa Arduino.
- **Monitorització:** Podeu monitoritzar els nivells d'humitat i el nivell del dipòsit a través de la consola sèrie de l'Arduino IDE.
- **Configuració:** Podeu actualitzar les configuracions de la humitat i altres paràmetres des de Firebase.

## Contribució

Si voleu contribuir al projecte, si us plau, seguiu aquests passos:

1. **Fork el repositori**
2. **Creeu una branca nova:**
   ```sh
   git checkout -b la_vostra_branca
3. **Feu els vostres canvis i feu commit:**
   ```sh
   git commit -m "Afegeix la teva descripció aquí"
4. **Pugeu els canvis a la vostra branca:**
   ```sh
   git push origin la_vostra_branca
5. **Creeu una pull request a GitLab**

## Llicència

Aquest projecte està llicenciat sota la Llicència GNU GPLv3. Consulteu el fitxer LICENSE per obtenir més informació.

