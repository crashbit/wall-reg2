#include "WiFiManager.h"

void WiFiManager::initialize() {
    int status = WL_IDLE_STATUS;
    while (status != WL_CONNECTED) {
        status = WiFi.begin(SECRET_SSIDF, SECRET_PASSF);
        delay(100);
    }
}

void WiFiManager::checkConnection() {
    if (WiFi.status() != WL_CONNECTED) {
        reconnect();
    }
}

void WiFiManager::reconnect() {
    while (WiFi.status() != WL_CONNECTED) {
        WiFi.begin(SECRET_SSIDF, SECRET_PASSF);
        delay(5000);
    }
}
