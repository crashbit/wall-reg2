#include "MoistureSensor.h"

MoistureSensor::MoistureSensor() : pin(-1) {}

MoistureSensor::MoistureSensor(int pin) : pin(pin) {}

void MoistureSensor::initialize() {
    pinMode(pin, INPUT);
}

int MoistureSensor::readSensor() {
    int readings[5];
    for (int i = 0; i < 5; i++) {
        readings[i] = analogRead(pin);
        delay(50);
    }
    int sum = 0;
    for (int i = 0; i < 5; i++) {
        sum += readings[i];
    }
    return sum / 5;
}

int MoistureSensor::read() {
    return readSensor();
}

