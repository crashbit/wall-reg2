#ifndef WIFIMANAGER_H
#define WIFIMANAGER_H

#include <WiFiNINA.h>
#include "arduino_secrets.h"

class WiFiManager {
public:
    void initialize();
    void checkConnection();

private:
    void reconnect();
};

#endif

