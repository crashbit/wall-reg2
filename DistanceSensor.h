#ifndef DISTANCESENSOR_H
#define DISTANCESENSOR_H

#include <Arduino.h>
#include <SoftwareSerial.h>

class DistanceSensor {
public:
    DistanceSensor();
    void initialize();
    int getAverageDistance();

private:
    SoftwareSerial mySerial;
    int readDistance();
};

#endif
