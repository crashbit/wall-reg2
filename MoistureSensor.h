#ifndef MOISTURESENSOR_H
#define MOISTURESENSOR_H

#include <Arduino.h>

class MoistureSensor {
public:
    MoistureSensor();
    MoistureSensor(int pin);
    void initialize();
    int read();

private:
    int pin;
    int readSensor();
};

#endif

