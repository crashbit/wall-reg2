#include "WaterPump.h"

WaterPump::WaterPump() : pin(-1), active(false), openTime(0), currentHumidity(0), lastHumidity(0) {}

WaterPump::WaterPump(int pin) : pin(pin), active(false), openTime(0), currentHumidity(0), lastHumidity(0) {}

void WaterPump::initialize() {
    pinMode(pin, OUTPUT);
    digitalWrite(pin, HIGH);
}

void WaterPump::activate() {
    digitalWrite(pin, LOW);
    active = true;
    openTime = millis();
    FirebaseManager::getInstance().setWaterPumpStatus(pin, true);
}

void WaterPump::deactivate() {
    digitalWrite(pin, HIGH);
    active = false;
    FirebaseManager::getInstance().setWaterPumpStatus(pin, false);
}

void WaterPump::checkTimeout() {
    if (active) {
        unsigned long currentTime = millis();
        if (currentTime - openTime > checkInterval) {
            if (currentHumidity <= lastHumidity) { // No ha augmentat la humitat
                deactivate();
            }
            lastHumidity = currentHumidity;
            openTime = currentTime;
        }
        if (currentTime - openTime > maxDuration) { // Ha passat massa temps
            deactivate();
        }
    }
}

bool WaterPump::isActive() {
    return active;
}

void WaterPump::updateHumidity(int humidity) {
    currentHumidity = humidity;
}
