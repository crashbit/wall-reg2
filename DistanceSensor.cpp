#include "DistanceSensor.h"

DistanceSensor::DistanceSensor() : mySerial(11, 10) {}

void DistanceSensor::initialize() {
    mySerial.begin(9600);
}

int DistanceSensor::readDistance() {
    unsigned char data[4] = {};
    int distance;
    int retry = 0;

    do {
        for (byte i = 0; i < 4; i++) {
            data[i] = mySerial.read();
        }
    } while (mySerial.read() == 0xff && retry++ < 5);

    if (retry >= 5) return -2;

    mySerial.flush();

    if (data[0] == 0xff) {
        int sum;
        sum = (data[0] + data[1] + data[2]) & 0x00FF;
        if (sum == data[3]) {
            distance = (data[1] << 8) + data[2];
            if ((distance > 30) && (distance < 245)) {
                return distance;
            } else if (distance >= 400) {
                return 0;
            } else {
                return 1;
            }
        } else {
            return -1;
        }
    }
    return -2;
}

int DistanceSensor::getAverageDistance() {
    int suma = 0;
    byte error = 0;
    byte buit = 0;
    int tmp = 0;
    byte count = 0;
    byte ple = 0;

    for (int x = 0; x < 20; x++) {
        tmp = readDistance();
        if (tmp == -1) {
            error += 1;
        } else if (tmp == 0) {
            buit += 1;
        } else if (tmp == 1) {
            ple += 1;
        } else if (tmp == -2) {
            x -= 1;
        } else {
            count += 1;
            suma += tmp;
        }
    }
    if (error > 10) {
        return -1;
    } else if (buit > 10) {
        return 0;
    } else if (ple > 10) {
        return 1;
    } else {
        return suma / count;
    }
}

