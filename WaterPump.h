#ifndef WATERPUMP_H
#define WATERPUMP_H

#include <Arduino.h>
#include "FirebaseManager.h"

class WaterPump {
public:
    WaterPump();
    WaterPump(int pin);
    void initialize();
    void activate();
    void deactivate();
    void checkTimeout();
    bool isActive();
    void updateHumidity(int humidity);

private:
    int pin;
    bool active;
    unsigned long openTime;
    int currentHumidity;
    int lastHumidity;
    const unsigned long maxDuration = 60000; // 1 minut
    const unsigned long checkInterval = 60000; // 1 minut
};

#endif
