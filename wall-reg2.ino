#include "WaterPumpSystem.h"

WaterPumpSystem waterSystem;

void setup() {
    waterSystem.setup();
}

void loop() {
    waterSystem.loop();
}

