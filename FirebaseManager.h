#ifndef FIREBASEMANAGER_H
#define FIREBASEMANAGER_H

#include "Firebase_Arduino_WiFiNINA.h"
#include "arduino_secrets.h"

class FirebaseManager {
public:
    static FirebaseManager& getInstance() {
        static FirebaseManager instance;
        return instance;
    }

    void initialize();
    void sendDiposit(int diposit);
    void sendData(byte sensor, int value);
    byte getFreq();
    int getMoistureLevel(int index);
    void setWaterPumpStatus(byte pump, bool status);
    void recordLastCheck();
    void updateConfig();

private:
    FirebaseManager() = default;
    FirebaseData fbdo;
    String path = "/torretes";
    bool retryOperation(bool (FirebaseManager::*operation)(), int maxRetries = 3);

    // Variables per als reintents
    int retryIntValue;
    byte retryByteValue;
    bool retryBoolValue;

    // Mètodes privats per als reintents
    bool setIntRetry();
    bool setBoolRetry();
    bool getIntRetry();
};

#endif
